import React from 'react' 
import PropTypes from 'prop-types'

import FormField from 'part:@sanity/components/formfields/default'
import TextInput from 'part:@sanity/components/textinputs/default'
import PatchEvent, {set, unset} from 'part:@sanity/form-builder/patch-event'
import { fixListingImageUrl } from './constants'

const createPatchFrom = value => PatchEvent.from(value === '' ? unset() : set(Number(value)))

export default class CloudinaryImage extends React.Component {
  static propTypes = {
    type: PropTypes.shape({
      title: PropTypes.string,
    }).isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
  };

  // this is called by the form builder whenever this input should receive focus
  focus() {
    this._inputElement.focus()
  }

  render() {
    const {type, value, onChange} = this.props
    let imgUrl = null;
    if (value) {
      // https://res.cloudinary.com/dwqo3od7a/image/upload/c_limit,h_200,w_200/v1529941989/automark_personal/j12wshaz7qaeu5rwttma.jpg
      if (value.includes('dwqo3od7a')) {
        imgUrl = fixListingImageUrl(value, 500, 500);
      } else if (value.includes('licdn')) {
        imgUrl = null;
      } else {
        imgUrl = `https://res.cloudinary.com/dwqo3od7a/image/upload/c_thumb,g_face,h_500,w_500/${value}`
      }
    }
    return (
      <FormField label={type.title}>
        <TextInput
          type="text"
          value={value === undefined ? '' : value}
          onChange={event => onChange(createPatchFrom(event.target.value))}
          ref={element => this._inputElement = element}
        />
        {imgUrl ? <img src={imgUrl} /> : null }
      </FormField>
    )
  }
}