export const fixListingImageUrl = (path, w, h) => {
  return path.split("/").slice(0, 6).concat([`c_thumb,g_face,h_${h},w_${w}`]).concat(path.split("/").slice(7)).join('/')
}