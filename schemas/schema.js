// First, we must import the schema creator
import React from "react";
import CloudinaryImage from "../components/CloudinaryImage";
import createSchema from "part:@sanity/base/schema-creator";

// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type";
import { fixListingImageUrl } from "../components/constants";

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "mySchema",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    {
      // This is the display name for the type
      title: "Profiles",

      // The identifier for this document type used in the api's
      name: "profile",

      // Documents have the type 'document'. Your schema may describe types beyond documents
      // but let's get back to that later.
      type: "document",

      // Now we proceed to list the fields of our document
      fields: [
        // This document has only one field
        {
          // The display name for this field
          title: "Name",

          // The identifier for this field used in the api's
          name: "name",

          // The type of this field
          type: "string"
        },
        {
          title: "Slug",
          name: "slug",
          type: "string"
        },
        {
          title: "Phone Number",
          name: "phone_number",
          type: "string"
        },
        {
          title: "Email",
          name: "email",
          type: "string"
        },
        {
          title: "Headline",
          name: "headline",
          type: "string"
        },
        {
          title: "Industry",
          name: "industry",
          type: "string"
        },
        {
          title: "Picture",
          name: "picture",
          type: "string",
          inputComponent: CloudinaryImage
        },
        {
          title: "Current Job",
          name: "current_job_company",
          type: "string",
        },
        {
          title: "Current Job - BETA",
          name: "current_job_company_beta",
          type: "reference",
          weak: true,
          to: [{ type: "company" }]
        },
        {
          title: "Current Job Description",
          name: "current_job_desc",
          type: "string"
        },
        {
          title: "Summary",
          name: "summary",
          type: "text"
        },
        {
          title: "Location",
          name: "location",
          type: "string"
        },
        {
          title: "Resume",
          name: "file_name",
          type: "string"
        },
        {
          title: "Current Job Department",
          name: "current_job_department",
          type: "string"
        },
        {
          title: "Employed",
          name: "employed",
          type: "boolean"
        },
        {
          title: "Experience In Years",
          name: "experience",
          type: "number"
        },
        {
          title: "Sector",
          name: "sector",
          type: "string",
          options: {
            list: [
              { title: "Manufacturing", value: "manufacturing" },
              { title: "Sales", value: "sales" }
            ]
          }
        },
        {
          title: "Interested In",
          name: "interested_in",
          type: "string"
        },
        {
          title: "Firebase User Id",
          name: "uid",
          type: "string"
        }
      ],
      preview: {
        select: {
          title: "name",
          subtitle: "current_job_company",
          description: "headline",
          picture: "picture"
          // if the movie has a director, follow the relation and get the name
        },
        prepare(selection) {
          const { title, subtitle, description, picture } = selection;

          let imgUrl = null;
          if (picture) {
            // https://res.cloudinary.com/dwqo3od7a/image/upload/c_limit,h_200,w_200/v1529941989/automark_personal/j12wshaz7qaeu5rwttma.jpg
            if (picture.includes("dwqo3od7a")) {
              imgUrl = fixListingImageUrl(picture, 40, 40);
            } else if (picture.includes("licdn")) {
              imgUrl = null;
            } else {
              imgUrl = `https://res.cloudinary.com/dwqo3od7a/image/upload/c_thumb,g_face,h_40,w_40/${picture}`;
            }
          }

          return {
            title,
            subtitle,
            description,
            media: imgUrl ? <img src={imgUrl} /> : null
          };
        }
      }
    },
    {
      title: "Company",
      name: "company",
      type: "document",
      fields: [
        {
          title: "Name",
          name: "name",
          type: "string"
        },

        {
          title: "ShortCode",
          name: "shortcode",
          type: "string"
        }
      ]
    }
  ])
});
